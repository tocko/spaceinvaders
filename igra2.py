import pygame
import matrica as m

ime_programa = "SPACE INVADERS  "
velicina_ekrana_x = 1000
velicina_ekrana_y = 500
pozadina_ekrana = (0, 0, 0)
def nacrtaj(el, ekran, x, y, sirina, visina):
    boja=()
    if el==4:
        boja=(0,255,0)
    if el==1:
        boja=(0,0,255)
    if el==8:
        boja=(255,200,0)
    if el==2:
        boja=(255,0,0)
    if el==16:
        boja=(255,100,50)
    if el==32:
        boja=(102, 102, 255)
    if el==64:
        boja=(153, 153, 102)
    if el==128:
        boja=(255, 255, 204)
    pygame.draw.rect(ekran, boja, (x, y, sirina, visina))

def crtaj_matricu(matrica, ekran):
    sirina = velicina_ekrana_x / m.sirina_mape
    visina = velicina_ekrana_y / m.visina_mape
    for x in range(m.sirina_mape):
        for y in range(m.visina_mape):
            el = matrica[y][x]
            if el != 0:
                nacrtaj(el, ekran, x * sirina, y * visina, sirina, visina)

def crtaj_toplinu(toplina,ekran):
    pygame.draw.rect(ekran,(255,255,255),(0,0,125,50))
    pygame.draw.rect(ekran,(255,0,0),(130,0,200,50))
    pygame.draw.rect(ekran,(0,255,0),(130,0,m.zdravlje*10,50))
    pygame.draw.rect(ekran,(255,0,0),(3,3,int(toplina),45))


def main():
    pygame.init()
    pygame.display.set_caption(ime_programa)
    pygame.key.set_repeat(30, 30)
    ekran = pygame.display.set_mode((velicina_ekrana_x, velicina_ekrana_y))
    print(type(ekran))
    m.program_radi = True
    pygame.time.set_timer(pygame.USEREVENT, 30)
    ispisF3=False
    pauza=0
    while m.program_radi: 
        # ovdje se dodaje crtanje
        ekran.fill(pozadina_ekrana)
        # ovde ostatak crtanja
        matrica = m.matrica_igre()
        crtaj_matricu(matrica, ekran)
        crtaj_toplinu(m.toplina,ekran)
        pygame.display.flip()
        e = pygame.event.wait()
        if e.type == pygame.USEREVENT:
            m.unisti_svemirca(m.bombe,m.kol)
            m.unisti_brod(m.vbombe,m.pozicija_broda)
            # napravi nesto na stopericu
            m.korak()
            if pauza>0:
                pauza=pauza-1
            if ispisF3:
                m.stanje_igre()
            if m.zdravlje==0 or m.lis==[False,False,False,False,False,False,False,False,False,False]:
                m.program_radi=False
            
        tipke = pygame.key.get_pressed()
        if tipke[pygame.K_q]:
            m.program_radi = False
        if tipke[pygame.K_LEFT]:
            m.brod_lijevo()
        if tipke[pygame.K_RIGHT]:
            m.brod_desno()
        if tipke[pygame.K_RETURN]:
            m.baci_bombu()
        if tipke[pygame.K_F3]: 
            if ispisF3 and pauza==0:
                 ispisF3=False
                 pauza=10
            if not ispisF3 and pauza==0:
                 ispisF3=True
                 pauza=10 


main()