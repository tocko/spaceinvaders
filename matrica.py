from copy import deepcopy

# globalne konstante
sirina_mape = 100
visina_mape = 50
visina_broda = visina_mape - 1
pauza_medju_bombama = 7

# pomocne funkcije
def prazna_matrica():
    mat = []
    for i in range(visina_mape):
        lis = []
        for j in range(sirina_mape):
            lis.append(0)
        mat.append(lis)
    return mat

def eksplozija(x,y):
    if x==None:
        return[(0,0)]
    return [
        (x,y),

        (x+1,y),
        (x,y+1),
        (x-1,y),
        (x,y-1),

        (x+1,y+1),
        (x-1,y-1),
        (x+1,y-1),
        (x-1,y+1)
    ]

def unisti_svemirca(bombe,kol):
    global lis
    global xe
    global ye
    global timer
    for i in range(10):
        for j in bombe.values():
            if j[1]==6:
                if (i*10+kol-1==j[0] or i*10+kol==j[0] or i*10+kol+1==j[0]) and lis[i]:
                    lis[i]=False
                    xe=j[0]
                    ye=j[1]
                    timer=5


def unisti_brod(vbombe,pozicija_broda):
    global zdravlje
    for i in vbombe.values():
        if i[1]==visina_broda:
                if i[0]-1==pozicija_broda or i[0]==pozicija_broda or i[0]+1==pozicija_broda:
                    zdravlje=zdravlje-1

def napravi_zid(x, y):
    return [
        (x, y),
        (x+1, y),
        (x+2, y),
        (x+3, y),
        (x+4, y),
        (x+5, y),
        (x+6, y),
        (x+7, y),
        (x+8, y),
        (x+9, y),

        (x, y+1),
        (x+1, y+1),
        (x+2, y+1),
        (x+3, y+1),
        (x+4, y+1),
        (x+5, y+1),
        (x+6, y+1), 
        (x+7, y+1),
        (x+8, y+1),
        (x+9, y+1),

        (x, y+2),
        (x+9, y+2),
    ]

def napravi_zidove():
    y = visina_mape - 6
    return napravi_zid(6*2, y) + napravi_zid(17*2, y) + napravi_zid(28*2, y) + napravi_zid(39*2, y)

def brod(x, y):
    return [
        (x-1,y),
        (x+1,y),
        (x,y-1)
    ]

def lista_bombi():
    global bombe
    return bombe.values()



def vlista_bombi():
    global vbombe
    return vbombe.values()

def dodaj_matrici(matrica, stvar, broj):
    for (x, y) in stvar:
        matrica[y][x] = broj

# akcije u igri
def vbaci_bombu():
    global vbombe
    global vbrojac_bombi
    global kol
    for i in range(10):
        if lis[i]:
            vbombe[vbrojac_bombi] = (i*10+kol, 8)
            vbrojac_bombi = vbrojac_bombi + 1
def brod_lijevo():
    global pozicija_broda
    if pozicija_broda > 1:
        pozicija_broda = pozicija_broda - 1

def brod_desno():
    global pozicija_broda
    if pozicija_broda < sirina_mape - 2:
        pozicija_broda = pozicija_broda + 1

def baci_bombu():
    global od_prosle_bombe
    global bombe
    global pozicija_broda
    global brojac_bombi
    global toplina
    if od_prosle_bombe < toplina:
        return
    bombe[brojac_bombi] = (pozicija_broda, visina_broda - 2)
    od_prosle_bombe = 0
    brojac_bombi = brojac_bombi + 1
    toplina=2**toplina

def svemirci(x,y):
    return [
        [x,y],
        [x+1,y],
        [x-1,y],
        [x,y+1]
    ]

def svemircimj(kol,lis):
    lis2=[]
    for i in range(10):
        if lis[i]:
            lis2=lis2+svemirci(i*10+kol,6)
    return lis2

def korak():
    global najv
    global pauza_medju_bombama
    global pozicija_broda
    global bombe
    global matrica
    global brojac_bombi
    global od_prosle_bombe
    global toplina
    global kol
    global strana
    global makni
    global xe
    global ye
    global timer
    if timer==0:
        xe=None
        ye=None
    else:
        timer=timer-1
    if makni==0:
        if kol==8:
            vbaci_bombu()
            strana=2
        if strana==2:
            kol=kol-1
        if kol==0:
            vbaci_bombu()
            strana=1
        if strana==1:
            kol=kol+1
        makni=1
    else:
        makni=(makni+1)%4
    if najv<toplina:
        najv=toplina
    if od_prosle_bombe < pauza_medju_bombama:
        od_prosle_bombe = od_prosle_bombe + 1
    izbrisi = []
    for k,(x,y) in bombe.items():
        pozadina = matrica[y][x]
        if pozadina == 4:
            izbrisi.append(k)
            matrica[y][x] = 0
        elif y == 0:
            izbrisi.append(k)
    for k in izbrisi:
        del bombe[k]
    izbrisi=[]
    for k,(x,y) in bombe.items():
        bombe[k] = (x, y-1)
    for k,(x,y) in vbombe.items():
        pozadina = matrica[y][x]
        if pozadina == 4:
            izbrisi.append(k)
            matrica[y][x] = 0
        elif y == visina_mape-1:
            izbrisi.append(k)
    for k in izbrisi:
        del vbombe[k]
    for k,(x,y) in vbombe.items():
        vbombe[k] = (x, y+1)
    if toplina>1:
        toplina=toplina/1.2
        if toplina < 1:
            toplina = 1
        #toplina=toplina - 0.4


def matrica_igre():
    global kol
    matrica_nova = deepcopy(matrica)
    if zdravlje>10:
        dodaj_matrici(matrica_nova, brod(pozicija_broda, visina_broda), 1)
    else:
        dodaj_matrici(matrica_nova, brod(pozicija_broda, visina_broda), 128)
    if zdravlje>10:
        dodaj_matrici(matrica_nova, [(pozicija_broda, visina_broda)], 32)
    else:
        dodaj_matrici(matrica_nova, [(pozicija_broda, visina_broda)], 64)
    dodaj_matrici(matrica_nova, lista_bombi(), 8)
    dodaj_matrici(matrica_nova, vlista_bombi(), 8)
    dodaj_matrici(matrica_nova, svemircimj(kol,lis), 2)
    dodaj_matrici(matrica_nova, eksplozija(xe,ye), 16)
    return matrica_nova


# za debug
def broj_u_znak(br):
    if br == 0:
        return " "
    else:
        return str(br)

def crtaj_matricu(matrica):
    for red in matrica:
        print("".join(map(broj_u_znak, red)))

def stanje_igre():
    print("Brod je na poziciji x-"+str(pozicija_broda)+" y-"+str(visina_broda)+".")
    print("Mitraljezi su topli "+str(toplina)+" stupnjeva.")
matrica_pocetak = prazna_matrica()
dodaj_matrici(matrica_pocetak, napravi_zidove(), 4)

# varijable igre
najv=0
program_radi = True
pozicija_broda = sirina_mape // 2
bombe = {}
matrica = matrica_pocetak
brojac_bombi= 0
od_prosle_bombe = pauza_medju_bombama
toplina=1
zdravlje=20
kol=1
strana=1    
makni=0
vbombe={}
vbrojac_bombi=0
puca=0
lis=[True,True,True,True,True,True,True,True,True,True]
xe=None
ye=None
timer=0
# main za testiranje



""" 
igra = igra_pocetak
matrica = matrica_igre(igra)
crtaj_matricu(matrica)
brod_lijevo(igra)

brod_lijevo(igra)
brod_lijevo(igra)
brod_lijevo(igra)
brod_lijevo(igra)
brod_lijevo(igra)
brod_lijevo(igra)

baci_bombu(igra)
matrica = matrica_igre(igra)
crtaj_matricu(matric0

korak(igra)
matrica = matrica_igre(igra)
crtaj_matricu(matrica)

korak(igra)
matrica = matrica_igre(igra)
crtaj_matricu(matrica)

for i in range(10):
    korak(igra)

baci_bombu(igra)
for i in range(10):
    korak(igra)

matrica = matrica_igre(igra)
crtaj_matricu(matrica)
 """
""" 
for i in range(10):
    korak(igra)


brod_desno(igra)
brod_desno(igra)
brod_desno(igra)
brod_desno(igra)
baci_bombu(igra)
matrica = matrica_igre(igra)
crtaj_matricu(matrica)

for i in range(10):
    korak(igra)
matrica = matrica_igre(igra)
crtaj_matricu(matrica)
 """

